package com.texoit.challenge.repository;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.texoit.challenge.model.MovieEntity;
import com.texoit.challenge.model.MovieResponse;

public interface MovieRepository extends JpaRepository<MovieEntity, Long>{
	
	@Query(name = MovieEntity.LONGEST_INTERVAL_BETWEEN_TWO_AWARDS)
	public MovieResponse findLongestIntervalBetweenPrizes(PageRequest pageRequest);
	
	@Query(name = MovieEntity.SHORTEST_INTERVAL_BETWEEN_TWO_AWARDS)
	public MovieResponse findShortestIntervalBetweenPrizes(PageRequest pageRequest);

}
