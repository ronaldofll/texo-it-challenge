package com.texoit.challenge.model;

import com.opencsv.bean.CsvBindByName;

public class Movie {
	
	@CsvBindByName(column = "year", required = false)
	private Integer year;
	
	@CsvBindByName(column = "title", required = false)
	private String title;

	@CsvBindByName(column = "studios", required = false)
	private String studios;

	@CsvBindByName(column = "producers", required = false)
	private String producers;

	@CsvBindByName(column = "winner", required = false)
	private String winner;
	
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStudios() {
		return studios;
	}
	public void setStudios(String studios) {
		this.studios = studios;
	}
	public String getProducers() {
		return producers;
	}
	public void setProducers(String producers) {
		this.producers = producers;
	}
	public String getWinner() {
		return winner;
	}
	public void setWinner(String winners) {
		this.winner = winners;
	}
	
	

}
