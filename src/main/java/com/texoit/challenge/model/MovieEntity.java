package com.texoit.challenge.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@NamedQuery(name = MovieEntity.LONGEST_INTERVAL_BETWEEN_TWO_AWARDS, query = "select new com.texoit.challenge.model.MovieResponse("
		+ "m1.producers, (m2.year - m1.year), m1.year, m2.year) "
		+ "FROM MovieEntity m1, MovieEntity m2 "
		+ "WHERE m1.producers = m2.producers "
		+ "AND m1.year < m2.year "
		+ "AND m1.winners = true and m2.winners = true "
		+ "AND NOT EXISTS (select m3 from MovieEntity m3 where m3.producers = m1.producers and m3.year > m1.year AND m3.year < m2.year AND m3.winners = true) "
		+ "order by (m2.year - m1.year) DESC")
@NamedQuery(name = MovieEntity.SHORTEST_INTERVAL_BETWEEN_TWO_AWARDS, query = "select new com.texoit.challenge.model.MovieResponse("
		+ "m1.producers, (m2.year - m1.year), m1.year, m2.year) "
		+ "FROM MovieEntity m1, MovieEntity m2 "
		+ "WHERE m1.producers = m2.producers "
		+ "AND m1.year < m2.year "
		+ "AND m1.winners = true and m2.winners = true "
		+ "AND NOT EXISTS (select m3 from MovieEntity m3 where m3.producers = m1.producers and m3.year > m1.year AND m3.year < m2.year AND m3.winners = true) "
		+ "order by (m2.year - m1.year)")
@Entity
public class MovieEntity {
	
	public static final String LONGEST_INTERVAL_BETWEEN_TWO_AWARDS = "MovieEntity.LONGEST_INTERVAL_BETWEEN_TWO_AWARDS";

	public static final String SHORTEST_INTERVAL_BETWEEN_TWO_AWARDS = "MovieEntity.SHORTEST_INTERVAL_BETWEEN_TWO_AWARDS";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Integer year;
	private String title;
	private String studios;
	private String producers;
	private Boolean winners;
	
	public void parser(Movie movie) { 
		this.year = movie.getYear();
		this.title = movie.getTitle();
		this.studios = movie.getStudios();
		this.producers = movie.getProducers();
		this.winners = movie.getWinner() != null ? "yes".equalsIgnoreCase(movie.getWinner()) : false;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStudios() {
		return studios;
	}
	public void setStudios(String studios) {
		this.studios = studios;
	}
	public String getProducers() {
		return producers;
	}
	public void setProducers(String producers) {
		this.producers = producers;
	}
	public Boolean getWinners() {
		return winners;
	}
	public void Boolean(Boolean winners) {
		this.winners = winners;
	}
	

}
