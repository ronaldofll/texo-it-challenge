package com.texoit.challenge.model;

public class MovieAwardIntervalResponse {
	
	private MovieResponse min;
	private MovieResponse max;
	
	public MovieResponse getMin() {
		return min;
	}
	public void setMin(MovieResponse min) {
		this.min = min;
	}
	public MovieResponse getMax() {
		return max;
	}
	public void setMax(MovieResponse max) {
		this.max = max;
	}

}
