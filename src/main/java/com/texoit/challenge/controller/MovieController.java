package com.texoit.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.challenge.model.MovieAwardIntervalResponse;
import com.texoit.challenge.service.MovieService;

@RestController
@RequestMapping("movie")
public class MovieController {
	
	@Autowired
	private MovieService service;
	
	@GetMapping("/award-interval")
	public MovieAwardIntervalResponse get() {
		MovieAwardIntervalResponse movieAwardIntervalResponse = service.listMovieAwardInterval();
		return movieAwardIntervalResponse;
	}
	
}

