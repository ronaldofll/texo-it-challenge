package com.texoit.challenge.tasks;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.texoit.challenge.service.MovieService;

public class ImportDAOTask implements Tasklet {
	
	private MovieService movieService;
	
	public ImportDAOTask(MovieService movieService) {
		this.movieService = movieService;
	}
	
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception
    {
        System.out.println("Iniciando task de importação de database CSV");

        movieService.importCSVDatabase();
         
        System.out.println("Finalizado task de importação de database em CSV.");
        return RepeatStatus.FINISHED;
    }

}
