package com.texoit.challenge.tasks;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.texoit.challenge.service.MovieService;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
	@Autowired
    private JobBuilderFactory jobs;
 
    @Autowired
    private StepBuilderFactory steps;

	@Autowired
	private MovieService movieService;
	
    @Bean
    public Step stepOne(){
        return steps.get("stepOne")
                .tasklet(new ImportDAOTask(movieService))
                .build();
    }
     
    @Bean
    public Job importJob(){
        return jobs.get("import-csv")
                .start(stepOne())
                .build();
    }

}
