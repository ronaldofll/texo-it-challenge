package com.texoit.challenge.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.opencsv.bean.CsvToBeanBuilder;
import com.texoit.challenge.model.Movie;
import com.texoit.challenge.model.MovieAwardIntervalResponse;
import com.texoit.challenge.model.MovieEntity;
import com.texoit.challenge.model.MovieResponse;
import com.texoit.challenge.repository.MovieRepository;

@Service
public class MovieServiceImpl implements MovieService {
	
	@Autowired
	private MovieRepository movieRepository;

	@Override
	public MovieAwardIntervalResponse listMovieAwardInterval() {
				
		MovieAwardIntervalResponse movieAwardIntervalResponse = new MovieAwardIntervalResponse();

		MovieResponse movieMaxInterval = movieRepository.findLongestIntervalBetweenPrizes(PageRequest.of(0, 1));
		MovieResponse movieMinInterval = movieRepository.findShortestIntervalBetweenPrizes(PageRequest.of(0, 1));
				
		movieAwardIntervalResponse.setMax(movieMaxInterval);
		movieAwardIntervalResponse.setMin(movieMinInterval);
		
		return movieAwardIntervalResponse;
	}

	@Override
	public Long insertMovie(Movie movie) {
		MovieEntity movieEntity = new MovieEntity();
		movieEntity.parser(movie);
		movieRepository.save(movieEntity);
		return movieEntity.getId();
	}

	@Override
	public MovieEntity findMovie(Long id) {
		Optional<MovieEntity> movieEntity = movieRepository.findById(id);
		if(movieEntity.isPresent()) {
			return movieEntity.get();
		} else {
			return null;
		}
	}
	
	@Override
	public void deleteMovie(Long id) {
		movieRepository.deleteById(id.longValue());
	}

	@Override
	public void importCSVDatabase() {

		try {
		    CsvToBeanBuilder<Movie> beanBuilder = new CsvToBeanBuilder<>(new InputStreamReader(new FileInputStream("movielist.csv")));

		    beanBuilder.withType(Movie.class);
		    beanBuilder.withSeparator(';');
		    beanBuilder.build().parse().forEach(new Consumer<Movie>() {

				@Override
				public void accept(Movie movie) {
					String[] producers = movie.getProducers().split(",|\\sand\\s");
					for(String producer : producers) {
						movie.setProducers(producer.trim());
						insertMovie(movie);	
					}
				}
			});

		} catch (FileNotFoundException e) {
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
		}
		
	}
}
