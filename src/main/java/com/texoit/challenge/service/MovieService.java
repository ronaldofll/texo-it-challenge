package com.texoit.challenge.service;

import com.texoit.challenge.model.Movie;
import com.texoit.challenge.model.MovieAwardIntervalResponse;
import com.texoit.challenge.model.MovieEntity;

public interface MovieService {

	public MovieAwardIntervalResponse listMovieAwardInterval();

	public void importCSVDatabase();

	Long insertMovie(Movie movie);

	MovieEntity findMovie(Long id);

	void deleteMovie(Long id);

}
