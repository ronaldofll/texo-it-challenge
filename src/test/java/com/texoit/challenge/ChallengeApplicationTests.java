package com.texoit.challenge;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.texoit.challenge.controller.MovieController;
import com.texoit.challenge.model.Movie;
import com.texoit.challenge.model.MovieEntity;
import com.texoit.challenge.service.MovieService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChallengeApplicationTests {
	
	
	private MockMvc mockMvc;
	
	@Autowired
	private MovieController movieController;
	
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(movieController).build();
	}

	@Autowired
	private MovieService movieService;

	@Test
	public void movieCrudTest() {
		Movie movie = new Movie();
		movie.setProducers("ProcucerTest");
		movie.setStudios("StudioTest");
		movie.setTitle("TitleTest");
		movie.setYear(2019);
		Long id = movieService.insertMovie(movie);
		
		assertNotNull(id);
		
		MovieEntity movieEntity = movieService.findMovie(id);
		assertNotNull(movieEntity);
		
		movieService.deleteMovie(id);
		
		movieEntity = movieService.findMovie(id);
		assertNull(movieEntity);
		
	}
	
	@Test
	public void integrationTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/movie/award-interval")).andExpect(MockMvcResultMatchers.status().isOk());
	}

}
