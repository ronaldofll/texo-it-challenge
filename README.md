

## EXECUÇÃO VIA ECLIPSE

1. Importar o projeto como projeto **maven**
2. executar a classe ChallengeApplication.java

---

## EXECUTAR VIA TERMINAL:

1. Executar o comando: `java -jar target/challenge-0.0.1-SNAPSHOT.jar` na pasta raíz do projeto

---

## EXECUTAR TESTES VIA ECLIPSE

1. executar a classe **ChallengeApplicationTests.java** com JUNIT

---

## EXECUTAR TESTE COM TERMINAL

1. executar o comando `mvn clean test` na pasta raíz do projeto

---

## OUTRAS INFORMAÇÕES:

1. Para testes também foi incluído o Swagger, que pode ser utilizado para rodar a API
	url para acesso ao swagger: http://localhost:8080/swagger-ui.html. Depois disso selecionar a API (/movie/award-interval) clicar em **Try it out** e depois em **Execute**

2. Para o banco em memória foi utilizado o H2.
para visualizar o banco de dados acessar a url **http://localhost:8080/h2** e clicar em connect


3. Para testes com comandos ou ferramentas externas a API ficará disponível em **http://localhost:8080/movie/award-interval**
